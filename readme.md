Poseidon
========

**Poseidon** is an open-source, extendable aquarium controller built with the Raspberry Pi and compatible hardware.

This branch (`master`) contains all the code for the Pi. Development shall be done in feature branches and merged back into master when tested and approved. 

The `pages` branch generates the [project blog](https://poseidonac.gitlab.io/poseidon) and is unrelated to the application code.